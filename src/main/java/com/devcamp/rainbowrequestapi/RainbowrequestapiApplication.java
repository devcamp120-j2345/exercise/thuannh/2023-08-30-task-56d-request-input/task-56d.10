package com.devcamp.rainbowrequestapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RainbowrequestapiApplication {

	public static void main(String[] args) {
		SpringApplication.run(RainbowrequestapiApplication.class, args);
	}

}
