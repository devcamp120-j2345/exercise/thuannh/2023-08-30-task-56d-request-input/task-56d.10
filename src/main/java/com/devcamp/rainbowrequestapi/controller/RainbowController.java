package com.devcamp.rainbowrequestapi.controller;

import java.util.ArrayList;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.Gson;

@RestController
@RequestMapping("/")
@CrossOrigin
public class RainbowController {
    private static final String[] rainbows = { "red", "orange", "yellow", "green", "blue", "indigo", "violet" };

    @GetMapping("/rainbow-request-query")
    public String getRainbowByFilter(@RequestParam("filter") String filter) {
        ArrayList<String> result = new ArrayList<>();
        for (String color : rainbows) {
            if (color.contains(filter)) {
                result.add(color);
            }
        }
        return new Gson().toJson(result);
    }

    @GetMapping("/rainbow-request-param/{index}")
    public String getRainbowByIndex(@PathVariable("index") int index) {
        if (index < 0 || index > 6) {
            return "{}";
        }
        return new Gson().toJson(rainbows[index]);
    }
}
